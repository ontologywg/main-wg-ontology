i# Main WG ontology
***
## Description
The AI Ontology Working Group is a group created by the projects and initiatives that are interested to connect or to align with the European AI-on-Demand (AIoD) platform. The main aim is to make knowledge discoverable across platforms and avoid replication of work across projects on ontology definition. Activity started in 2021 with a first series of workshops (within the umbrella of the AI4EU Technical Governance Board) with some ICT 49 projects (AI4Copernicus, AIPlan4EU, and I-Nergy) which had plans to extend (in different directions) the AI4EU conceptual semantic model to support new knowledge representation for the AI-on-demand platform. At the end of AI4EU the working group members continued the organization of meetings. Other initiatives such as European Language Grid (ELG) or other ICT 48 projects such as Tailor and AI4Media expressed their interest to participate in the working group.

Currently, the working group is composed of representatives from AI4Copernicus, AI4Media, AIPlan4EU, DIH4EU, ELG, I-NERGY, TAILOR and StairwAI.
## Contributing
Before contributing to this repository, contact with any of the defined contact points (see them below) and a branch will be created for you or your institution.

## Installation
Clone this repository to your local machine, change to your assigned branch and edit the ontology. Additionally, add a log in your README file with the changes done.

## Licence
Open source (temporally assigned)

## Project status
Currently, this ontology is edited under the description obtained after the Ontology Working Group meetings.

## Log 
Element | Description
:--- | :---
Date | 25-11-2022
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. The agenda of the meeting contains the presentation of how *AI Asset/AI Artifact/AI Technique* terms are modelled in AI4EU Conceptual Model and StairwAI. <br /> Some discussions raise during the meeting: <ul><li> About the equivalence between the Distribution and Dataset terms raise (ending status: set as two differentiated terms) </li><li> Disconnect Agent from Problem Statement relationship, giving this role to a subclass of Agent called *Adopter* or *Innovator* (ending status: the issue remains opened) </li></ul> 
TODO'S |  <ul><li> &#9745; Creation of a subclass for *Tool* called **Datatool** which is related to *Dataset* term (relation Datatool->Dataset is *obtains data from*).</li><li> &#9745; Set **AI expert** as a child class of *AI Asset* instead of *Person*. </li><li> &#9746; Change the **AI Asset** term definition (responsible not assigned). </li></ul>

Element | Description
:--- | :---
Date | 22-12-2022
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. Some discussions that raise during the meeting were: <ul><li> Joaquim Vanschoren shared a google doc file where the attendees can see how the AI4Europe data model was modelling (link to be add) </li><li> During the meeting, a search for archetypal problems with AI information source was proposed (not assigned to anyone). </li><li> Agent term was proposed to be changed to AI Agent.  </li></ul>
TODO'S | <ul><li> &#9745; Change Agent term to AI Agent </li> </ul>

Element | Description
:--- | :---
Date | 22-02-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. The aim of the session was to create an agreement for the final version of the aims and procedures file proposed by StairwAI. Some little amendments were done, and the final version was generated and can be found in the "extra-data" folder in the Ontology WG repository. In addition, a discussion about the Ontology editing tool that can be used raised, the final tool proposed can be seen in the above-mentioned file.
TODO'S | None

Element | Description
:--- | :---
Date | 01-03-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. During this meeting a first Ontology development and visialization tools were explored. In the following meetings each tool will be presented, exposing their pros and cons to all the assistents. 
TODO'S | None

Element | Description
:--- | :---
Date | 08-03-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. This meeting was devoted to expose Neo4j tool which stands to be a Ontology visualizer for the Ontology WG, but would not be considered as a ontology manager due to the modifications have to be done using queries.
TODO'S | None

Element | Description
:--- | :---
Date | 15-03-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. Charles Emehel introduce his promising tool to the meeting assistants. 
TODO'S | None

Element | Description
:--- | :---
Date | 22-03-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. Penny Labropoulou introduce the VocBench ontology editing tool, as a user of it.
TODO'S | None

Element | Description
:--- | :---
Date | 29-03-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. This meeting was devoted to set the responsible that will contact with the Graphology and VocBench tools in order to have a demo in the following meetings. Another topic discussed was the communication channel for the group, the members in the meeting accept the proposal to use Discord as the tool for communication.  
TODO'S | None

Element | Description
:--- | :---
Date | 05-04-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. In this meeting Mr Paul Appleby and Mr Revinder Singh explain the Graphology and Easygraph tools as a candidate to be the tool for the OntologyWG.   
TODO'S | None

Element | Description
:--- | :---
Date | 12-04-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. This meeting had as its main theme the in-depth exposure of the VocBench tool by one of its main developers, Armando Stellato. 
TODO'S | None

Element | Description
:--- | :---
Date | 19-04-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. This meeting had the purpose of discussing the pros and cons of the two previously exposed tools (VocBench and Graphology). Aspects such as pricing, usability and sustainability were evaluated. On the other hand, an attempt was made to use the communication tool Discord, in which some permission problems were detected, which were later solved. 
TODO'S | None

Element | Description
:--- | :---
Date | 26-04-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date.  
TODO'S | None

Element | Description
:--- | :---
Date | 03-05-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. This meeting was devoted to restart the ontological conversations in addition to have a practical usage of VocBench. Mainly, the discussion was centred on AI Taxonomy, in which all the group agrees that will require changes.
TODO'S | <ul><li> (Miquel) &#9745; Merge all the ontologies used in StairwAI in one. </li> <li> (Coordinators) &#9745; Create a Discord specific conversation to discuss about the AI Taxonomy issues. </li></ul>

Element | Description
:--- | :---
Date | 09-05-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. This extraordinary meeting was scheduled on Tuesday, in which Daniel Fiebig and Georg Rehm presented the environment Field33 which is proposed as a candidate to become a tool for the OntologyWG. 
TODO'S | None

Element | Description
:--- | :---
Date | 10-05-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. No data from this meeting.
TODO'S | None

Element | Description
:--- | :---
Date | 17-05-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. A first proposal of ontological terms were discussed, overall there are two main sources of information AI watch and DIN.
TODO'S | None

Element | Description
:--- | :---
Date | 24-05-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. A first list of AI ontological terms were proposed and listed in a spreadsheet.
TODO'S | <ul><li> (All the participants) &#9745; From the terms proposed in the spreadsheet, define them using referenced definitions. </li></ul>

Element | Description
:--- | :---
Date | 31-05-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. In this meeting, the analysis of the terms and definitions wrote during the week stated. After a first analysis, some of the definitions were rejected.
TODO'S | <ul><li> (Miquel) &#9745; Create a new ontology with the terms analysed, its definitions and some subterms extracted from the files from where the chosen definitions were extracted. </li></ul>

Element | Description
:--- | :---
Date | 07-06-2023
Author | Miquel Buxons
Changes | This log corresponds to the Ontology Working group meeting that takes place the same day of the log date. In this meeting, The rest of the terms were added to the ontology RDF. Additionally, the assistants decided to choose VocBench as the collaborative edition of the resultant ontology.
TODO'S | <ul><li> (Miquel) &#9745; Contact with Penny in order to start the procedure to log in all the participants to VocBench. </li></ul>

Element | Description
:--- | :---
Date | 20-12-2023
Author | Miquel Buxons
Changes | Costas and Antonis presented an Excel document (which can be found in the extra-data folder) demonstrating a prototype taxonomy developed in the context of the Network of Excellence. This categorization was shared for the purpose of allowing meeting participants to access the document, familiarize themselves with its contents, and suggest modifications.
TODO'S | <ul><li>  </li></ul>

Element | Description
:--- | :---
Date | 21-01-2024
Author | Miquel Buxons
Changes | Costas and Antonis led the discussion regarding the document presented at the previous meeting. Costas mentioned his intention to split the current 'Application Areas' column into two distinct categories: 'Business Sectors' and 'Archetypical Application Areas.' During the discussion, it was noted that the document lacked definitions for certain terms, and it was suggested that providing these definitions would be beneficial. Costas stated he would inquire about disseminating these missing definitions.
TODO'S | <ul><li> Costas and Antonis offered to make available to the group members the information on the user journey template set for completion. </li></ul>


# StairwAI ontology Log
Description of the changes done over StairwAI ontology after the last deliverable (month 18). The changes can be specified by project internal requests or by requests specified inside THE Ontology WG.
## Internally suggested changes
Date | Description
:--- | :---
10-2022 | Add new and extense descriptions to main AI Tehniques (based on the hierarchy level).
12-2022 | Implementation of unique IDs for each nodes, inserted in the AMS based on Neo4j identifiers, which are not consistent over Reset but unique. 
12-2022 | Dataproperty created to handle the IDs.
12-2022 | Node search engine based on name-class tuple changed to one based on IDs (identity extracted from Neo4j)
11-2022 | AMS Back-up funtionalities implemented.
12-2022 | Time optimization to modify/insert instance methods.
01-2023 | Add AI Expert specific support functionalities to StairwAI AMS.
02-2023 | Add new and extense descriptions to all business categories.
02-2023 | Modify AMS methods which extract AI Techniques and Business categories to easily retrieve the term descriptions.
02-2023 | Insert unique UUID identifiers during the instance insertions. This instance attibute was set as a Dataproperty in the ontology.
02-2023 | Node2data output fields labels adapted and standarized in agreement with Michele.
02-2023 | Add **team size** data property addition to AI Experts, as a requirement of WP5 matchmaking.
03-2023 | Request per utilitzar/adaptar Solution as a Usecases.
04-2023 | Addition of automatic backup function, the suggestions include the requirement to set a specific hour for the backup and require the path to store the files.
04-2023 | modify_instance function modified to concurrently accept, property deletes and property addition.
04-2023 | Addition of price as a Data property for AI Experts in the StairwAI ontology.

## Ontology WG suggested changes 
Date | Description
:--- | :---
25-11-2022 |  Creation of a subclass for *Tool* called **Datatool** which is related to *Dataset* term (relation Datatool->Dataset is *obtains data from*).
25-11-2022 |  **AI expert** as a child class of *AI Asset* instead of *Person*. 
25-11-2022 |  Change the **AI Asset** term definition (responsible not assigned).
22-12-2022 |  Change Agent term to AI Agent.
02-05-2023 |  Merge all the ontologies (main StairwAI ontology and the auxiliar taxonomies) into one single ontology.  
31-05-2023 |  Create a new ontology with only the terms discussed and its sub-terms.
07-06-2023 |  Insertion of part of and enables relations.

## Contact points

- miquel.buxons@upc.edu
- javier.vazquez-salceda@upc.edu
- javier.farreres@upc.edu
